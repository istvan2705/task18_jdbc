package com.kish.entity;

public class Customer {
    private int id;
    private String name;
    private String surname;
    private long taxNumber;
    private String phoneNumber;
    private Agency agency;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public long getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(long taxNumber) {
        this.taxNumber = taxNumber;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (id != customer.id) return false;
        if (taxNumber != customer.taxNumber) return false;
        if (!name.equals(null) ? !name.equals(customer.name) : !customer.name.equals(null)) return false;
        if (!surname.equals(null) ? !surname.equals(customer.surname) : !customer.surname .equals(null)) return false;
        if (!phoneNumber .equals(null)? !phoneNumber.equals(customer.phoneNumber) : customer.phoneNumber.equals(null))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * (int) (taxNumber ^ (taxNumber >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", taxNumber=" + taxNumber +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", agency=" + agency +
                '}';
    }
}
