package com.kish.entity;

public class RealEstate {
    private int id;
    private String address;

   public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public EstateBusinessPurpose estBusinessPurpose;
    public EstatePurpose estatePurpose;

    public EstateBusinessPurpose getEstBusinessPurpose() {
        return estBusinessPurpose;
    }

    public void setEstBusinessPurpose(EstateBusinessPurpose estBusinessPurpose) {
        this.estBusinessPurpose = estBusinessPurpose;
    }

    public EstatePurpose getEstatePurpose() {
        return estatePurpose;
    }

    public void setEstatePurpose(EstatePurpose estatePurpose) {
        this.estatePurpose = estatePurpose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealEstate realEstate = (RealEstate) o;

        if (id != realEstate.id) return false;
        if (!address.equals(null) ? !address.equals(realEstate.address) : !realEstate.address.equals(null)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RealEstate{" +
                "id=" + id +
                ", address='" + address + '\'' +
                '}';
    }
}
