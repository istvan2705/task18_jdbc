package com.kish.entity;

public class OverdueContract {
    private int id;
    private double overduePaymentAmount;
    private int numberOfDaysOverdue;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public double getOverduePaymentAmount() {
        return overduePaymentAmount;
    }

    public void setOverduePaymentAmount(double overduePaymentAmount) {
        this.overduePaymentAmount = overduePaymentAmount;
    }

    public int getNumberOfDaysOverdue() {
        return numberOfDaysOverdue;
    }

    public void setNumberOfDaysOverdue(int numberOfDaysOverdue) {
        this.numberOfDaysOverdue = numberOfDaysOverdue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OverdueContract that = (OverdueContract) o;

        if (id != that.id) return false;
        if (Double.compare(that.overduePaymentAmount, overduePaymentAmount) != 0) return false;
        if (numberOfDaysOverdue != that.numberOfDaysOverdue) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(overduePaymentAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + numberOfDaysOverdue;
        return result;
    }
}
