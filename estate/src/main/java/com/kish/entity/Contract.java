package com.kish.entity;

import java.sql.Date;

public class Contract {
    private int id;
    private Date dateOfContract;
    private Date validUntil;
    private double contractAmount;
    private Customer customer;
    private RealEstate realEstate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfContract() {
        return dateOfContract;
    }

    public void setDateOfContract(Date dateOfContract) {
        this.dateOfContract = dateOfContract;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public double getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(double contractAmount) {
        this.contractAmount = contractAmount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public RealEstate getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(RealEstate realEstate) {
        this.realEstate = realEstate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        if (id != contract.id) return false;
        if (Double.compare(contract.contractAmount, contractAmount) != 0) return false;
        if (!dateOfContract.equals(null) ? !dateOfContract.equals(contract.dateOfContract) : !contract.dateOfContract.equals(null))
            return false;
        if (!validUntil.equals(null) ? !validUntil.equals(contract.validUntil) : !contract.validUntil.equals(null)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (dateOfContract != null ? dateOfContract.hashCode() : 0);
        result = 31 * result + (validUntil != null ? validUntil.hashCode() : 0);
        temp = Double.doubleToLongBits(contractAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "id=" + id +
                ", dateOfContract=" + dateOfContract +
                ", validUntil=" + validUntil +
                ", contractAmount=" + contractAmount +
                ", customer=" + customer +
                ", realEstate=" + realEstate +
                '}';
    }
}
