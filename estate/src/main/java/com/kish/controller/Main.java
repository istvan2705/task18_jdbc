package com.kish.controller;

import com.kish.dao.*;
import com.kish.dao.implementation.*;
import com.kish.entity.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.apache.log4j.LogManager.getLogger;

public class Main {

    private static Logger log = getLogger(Main.class);
    public static void main(String[] args) throws SQLException {

        DaoFactory factory = new DAOFactoryImpl();
        Connection connection = factory.getConnection();
        AgencyDAO agencyDAO = new AgencyDaoImpl(connection);
        ContractDAO contractDAO = new ContractDaoImpl(connection);
        CustomerDAO customerDAO = new CustomerDaoImpl(connection);
        PaymentDAO paymentDAO = new PaymentDaoImpl(connection);
        RealEstateDAO realEstateDAO = new RealEstateDaoImpl(connection);

        //Agencies

        //list of agencies
        List<Agency> listOfAgencies = agencyDAO.getAll();
        listOfAgencies.stream().forEach(System.out::println);

        //create agency
        Agency agency = new Agency();
        agency.setName("Real Estate");
        agency.setAddress("Chuprynky, 13");
        agency.setPhoneNumber("0678401254");
        boolean isCreatedRow =agencyDAO.create(agency);
        log.info(isCreatedRow);

        //update agency
        agency.setPhoneNumber("0973455687");
        agency.setId(2);
        boolean isUpdatedRow = agencyDAO.update(agency);
        log.info(isUpdatedRow);

        //Contract

        //all contracts
        List<Contract> allContracts = contractDAO.getAll();
        allContracts.stream().forEach(System.out::println);

         //find by customer ID
        List<Contract> listGetByCustomerID =contractDAO.getContractByCustomerID(6);
        listGetByCustomerID.stream().forEach(System.out::println);

        //find by real_state_ID
        List<Contract> listOfContractsByEstate =contractDAO.getContractByRealEstateID(3);
        listOfContractsByEstate.stream().forEach(System.out::println);

       //Customer

        // findByTaxNumber
        Customer customerFoundByTaxNumber = customerDAO.getCustomerByTaxNumber(Double.valueOf(2142945832));
        log.info(customerFoundByTaxNumber);

        //all customers
        List<Customer> allCustomers = customerDAO.getAll();
        allCustomers.stream().forEach(System.out::println);

        //Payment

        //all payments
        List<Payment> allPayments = paymentDAO.getAll();
        allPayments.stream().forEach(System.out::println);

        //payments get By ContractId
        List<Payment> paymentsGetByContractID = paymentDAO.getListOfPaymentsByContractID(3);
        paymentsGetByContractID.stream().forEach(System.out::println);

        //payments get By CustomerId
        List<Payment> paymentsGetByCustomerID = paymentDAO.getListOfPaymentsByCustomerID(2);
        paymentsGetByCustomerID.stream().forEach(System.out::println);

        //joined table customer and payment
        List<Payment> paymentsOnJoinedTables =  paymentDAO.getPaymentsOnJoin();
        paymentsOnJoinedTables.stream().forEach(System.out::println);

        //RealEstate

        // all list of RealEstate
        List<RealEstate> allListOfRealEstate = realEstateDAO.getAll();
        allListOfRealEstate.stream().forEach(System.out::println);
    }



    public static java.sql.Date convert(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }
}



