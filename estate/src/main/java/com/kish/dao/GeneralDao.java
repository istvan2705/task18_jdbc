package com.kish.dao;

import java.sql.SQLException;
import java.util.List;

public interface GeneralDao<T> {
    List<T> getAll() throws SQLException;
   }
