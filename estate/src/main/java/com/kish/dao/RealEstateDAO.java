package com.kish.dao;

import com.kish.entity.EstateBusinessPurpose;
import com.kish.entity.RealEstate;

import java.sql.SQLException;
import java.util.List;

public interface RealEstateDAO extends GeneralDao<RealEstate> {
    List<RealEstate> getListOfRealEstateByEstatePurpose(Integer ID) throws SQLException;
    List<RealEstate> getListOfRealEstateByBusinessPurpose(Integer ID) throws SQLException;
    boolean create (RealEstate realEstate) throws SQLException;
}
