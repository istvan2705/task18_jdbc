package com.kish.dao;

import com.kish.entity.Agency;

import java.sql.SQLException;

public interface AgencyDAO extends GeneralDao<Agency> {
    boolean create(Agency agency) throws SQLException;
    boolean update(Agency agency) throws SQLException ;
}
