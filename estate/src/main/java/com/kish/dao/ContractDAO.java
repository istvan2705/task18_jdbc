package com.kish.dao;

import com.kish.entity.Contract;

import java.sql.SQLException;
import java.util.List;

public interface ContractDAO extends GeneralDao<Contract>{
    List<Contract> getContractByCustomerID(Integer ID) throws SQLException;
    List<Contract>getContractByRealEstateID(Integer ID) throws SQLException;
    boolean create(Contract contract) throws SQLException;
}
