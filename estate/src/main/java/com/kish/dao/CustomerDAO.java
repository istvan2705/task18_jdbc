package com.kish.dao;

import com.kish.entity.Customer;

import java.sql.SQLException;

public interface CustomerDAO extends GeneralDao<Customer> {

    Customer getCustomerByTaxNumber(Double taxNumber) throws SQLException;
    Customer getCustomerByPhoneNumber(String phoneNumber) throws SQLException;
     boolean update(Customer customer) throws SQLException;
     boolean create(Customer customer) throws SQLException;
}
