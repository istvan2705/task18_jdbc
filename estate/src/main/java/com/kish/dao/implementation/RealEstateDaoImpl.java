package com.kish.dao.implementation;

import com.kish.dao.RealEstateDAO;
import com.kish.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RealEstateDaoImpl implements RealEstateDAO {
    Connection connection;
    public static final String SELECT_QUERY = "SELECT id, address, estate_purpose_id, estate_business_purpose_id1" +
            " FROM mydb.real_estate";
    public static final String SELECT_BY_ESTATE_PURPOSE = SELECT_QUERY + "WHERE estate_purpose_id= ?";
    public static final String SELECT_BY_BUSINESS_PURPOSE = SELECT_QUERY + "WHERE estate_business_purpose_id1= ?";
    public static final String CREATE = "INSERT INTO mydb.real_estate (address, estate_purpose_id\n," +
            " estate_business_purpose_id1) VALUES (?, ?, ?)";


    public RealEstateDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<RealEstate> getListOfRealEstateByEstatePurpose(Integer ID) throws SQLException {

        return getRealEstates(ID, SELECT_BY_ESTATE_PURPOSE);
    }
    @Override
    public List<RealEstate> getListOfRealEstateByBusinessPurpose(Integer ID) throws SQLException {
        return getRealEstates(ID, SELECT_BY_BUSINESS_PURPOSE);
    }

    private List<RealEstate> getRealEstates(Integer ID, String selectByEstatePurpose) throws SQLException {
        List<RealEstate> listOfEstate = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(selectByEstatePurpose)) {
            statement.setInt(1, ID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                RealEstate realEstate = extractRealEstateFromResultSet(rs);
                listOfEstate.add(realEstate);
            }
        }
        return listOfEstate;
    }


    @Override
    public List<RealEstate> getAll() throws SQLException {

        List<RealEstate> listOfContracts = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_QUERY);
            while (rs.next()) {
                RealEstate realEstate = extractRealEstateFromResultSet(rs);
                listOfContracts.add(realEstate);
            }
        }
        return listOfContracts;
    }

    private RealEstate extractRealEstateFromResultSet(ResultSet rs) throws SQLException {

        RealEstate realEstate = new RealEstate();
        realEstate.setId(rs.getInt("id"));
        realEstate.setAddress(rs.getString("address"));
        EstatePurpose purpose = new EstatePurpose();
        purpose.setType(rs.getString("estate_purpose_id"));
        realEstate.setEstatePurpose(purpose);
        EstateBusinessPurpose businessPurpose = new EstateBusinessPurpose();
        businessPurpose.setType(rs.getString("estate_business_purpose_id1"));
        realEstate.setEstBusinessPurpose(businessPurpose);
        return realEstate;
    }

    @Override
    public boolean create(RealEstate realEstate) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setString(1, realEstate.getAddress());
            ps.setInt(2, realEstate.getEstatePurpose().getId());
            ps.setInt(3, realEstate.getEstBusinessPurpose().getId());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        }
        return false;
    }
}

