package com.kish.dao.implementation;

import com.kish.dao.CustomerDAO;
import com.kish.entity.Agency;
import com.kish.entity.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDaoImpl implements CustomerDAO {
    Connection connection;
    public static final String SELECT_QUERY = "SELECT id, surname, name, tax_number, phone_number, agency_id from mydb.customer";
    public static final String SELECT_BY_PHONE = SELECT_QUERY + " WHERE phone_number= ?";
    public static final String SELECT_BY_TAX_NUMBER = SELECT_QUERY + " WHERE tax_number= ?";
    public static final String CREATE_QUERY = "INSERT INTO mydb.customer (surname, name, tax_number, phone_number, agency_id) VALUES (?, ?, ?, ?, ?);";
    public static final String UPDATE_QUERY = "UPDATE mydb.customer SET phoneNumber= ?  WHERE ID= ?;";

       public CustomerDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Customer getCustomerByTaxNumber(Double taxNumber) throws SQLException {
        Customer customer = new Customer();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_BY_TAX_NUMBER)) {
            statement.setDouble(1, taxNumber);
            ResultSet rs = statement.executeQuery();
            parseResultSet(customer, rs);
            return customer;
        }
    }

    private void parseResultSet(Customer customer, ResultSet rs) throws SQLException {
        while (rs.next()) {
            setCustomerByDataResultSet(customer, rs);
        }
    }

    private void setCustomerByDataResultSet(Customer customer, ResultSet rs) throws SQLException {
        customer.setId(rs.getInt("id"));
        customer.setSurname(rs.getString("surname"));
        customer.setName(rs.getString("name"));
        customer.setTaxNumber(rs.getLong("tax_number"));
        customer.setPhoneNumber(rs.getString("phone_number"));
        Agency agency = new Agency();
        agency.setId(rs.getInt("agency_id"));
        customer.setAgency(agency);
    }

    @Override
    public Customer getCustomerByPhoneNumber(String phoneNumber) throws SQLException {
        Customer customer = new Customer();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_BY_PHONE)) {
            statement.setString(1, phoneNumber);
            ResultSet rs = statement.executeQuery();
            parseResultSet(customer, rs);
            return customer;
        }
    }

    @Override
    public List<Customer> getAll() throws SQLException {
        List<Customer> listOfCustomers = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_QUERY);
             while (rs.next()) {
                Customer customer = extractCustomerFromResultSet(rs);
                listOfCustomers.add(customer);
            }
        }
        return listOfCustomers;
    }

    private Customer extractCustomerFromResultSet(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        setCustomerByDataResultSet(customer, rs);
        return customer;
    }

    @Override
    public boolean create(Customer customer) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE_QUERY)) {
            ps.setString(1, customer.getSurname());
            ps.setString(2, customer.getName());
            ps.setDouble(3, customer.getTaxNumber());
            ps.setString(4, customer.getPhoneNumber());
            ps.setInt(5, customer.getAgency().getId());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean update(Customer customer) throws SQLException {
        try(PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
            ps.setString(1, customer.getPhoneNumber());
            ps.setInt(2, customer.getId());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        }
        return false;
    }
}
