package com.kish.dao.implementation;

import com.kish.dao.DaoFactory;
import com.kish.entity.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DAOFactoryImpl implements DaoFactory {
    private String user = "root";
    private String password = "1401198n";
    private String url = "jdbc:mysql://localhost:3306/mydb?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private String driver = "com.mysql.cj.jdbc.Driver";
    private Map<Class, DaoCreator> creators;

    public DAOFactoryImpl() {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        creators = new HashMap<>();
        creators.put(Agency.class, connection -> new AgencyDaoImpl(connection));
        creators.put(Contract.class, connection -> new ContractDaoImpl(connection));
        creators.put(Customer.class, connection -> new CustomerDaoImpl(connection));
        creators.put(Payment.class, connection -> new PaymentDaoImpl(connection));
        creators.put(RealEstate.class, connection -> new RealEstateDaoImpl(connection));
    }

    public Connection getConnection() throws SQLException {
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new SQLException();
        }
        return connection;
    }
}
