package com.kish.dao.implementation;

import com.kish.dao.ContractDAO;
import com.kish.entity.Contract;
import com.kish.entity.Customer;
import com.kish.entity.DBTablePrinter;
import com.kish.entity.RealEstate;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ContractDaoImpl implements ContractDAO {
    Connection connection;

    public static final String SELECT_QUERY = "SELECT id, date_of_contract, valid_until, contract_amount,\n" +
            "customer_id, real_estate_id FROM mydb.contract";
    public static final String SELECT_BY_CUSTOMER_ID = SELECT_QUERY+" WHERE customer_id= ?";
    public static final String SELECT_BY_REAL_ESTATE_ID = SELECT_QUERY +" WHERE real_estate_id= ?";
    public static final String CREATE_CONTRACT = "INSERT INTO mydb.contract (date_of_contract, valid_until,\n" +
            " contract_amount, customer_id, real_estate_id) VALUES (?, ?, ?, ?, ?)";
    public static final String JOIN_QUERY = "SELECT surname, contract_amount from mydb.customer inner\n" +
            " join mydb.contract on contract.customer_id = customer.id where customer.id is not null";


    public ContractDaoImpl(Connection connection) {
        this.connection = connection;
    }

    public List<Contract> getListOfContracts() throws SQLException {
        List<Contract> contracts = new ArrayList<>();
        String sql = JOIN_QUERY;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            //    DBTablePrinter.printResultSet(rs);
            while (rs.next()) {
                Contract contract = new Contract();
                contract.setCustomer(loadCustomer(rs));
                contract.setContractAmount(rs.getInt("contract_amount"));
                contracts.add(contract);
            }
        } catch (SQLException e) {
            throw new SQLException(e);
        }
        return contracts;
    }

    public Customer loadCustomer(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setSurname(rs.getString("surname"));
        return customer;
    }

    @Override
    public List<Contract> getContractByCustomerID(Integer ID) throws SQLException {
        return getContracts(ID, SELECT_BY_CUSTOMER_ID);
    }

    @Override
    public List<Contract> getContractByRealEstateID(Integer ID) throws SQLException {
        return getContracts(ID, SELECT_BY_REAL_ESTATE_ID);
    }

    private List<Contract> getContracts(Integer ID, String SQLQuery) throws SQLException {
        List<Contract> contracts = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQLQuery)) {
            statement.setInt(1, ID);
            ResultSet rs = statement.executeQuery();
            DBTablePrinter.printResultSet(rs);

            while (rs.next()) {
                Contract contract = extractContractFromResultSet(rs);
                contracts.add(contract);
            }
        }
        return contracts;
    }

    @Override
    public List<Contract> getAll() throws SQLException {
        List<Contract> listOfContracts = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_QUERY);
            DBTablePrinter.printResultSet(rs);
            while (rs.next()) {
                Contract contract = extractContractFromResultSet(rs);
                listOfContracts.add(contract);
            }
        }
        return listOfContracts;
    }

    public Contract extractContractFromResultSet(ResultSet rs) throws SQLException {

        Contract contract = new Contract();
        contract.setId(rs.getInt("id"));
        contract.setDateOfContract(rs.getDate("date_of_contract"));
        contract.setValidUntil(rs.getDate("valid_until"));
        contract.setContractAmount(rs.getDouble("contract_amount"));
        Customer customer = new Customer();
        customer.setId(rs.getInt("customer_id"));
        contract.setCustomer(customer);
        RealEstate realEstate = new RealEstate();
        realEstate.setId(rs.getInt("real_estate_id"));
        contract.setRealEstate(realEstate);
        return contract;
    }

    @Override
    public boolean create(Contract contract) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE_CONTRACT)) {
            ps.setDate(1, contract.getDateOfContract());
            ps.setDate(2, contract.getValidUntil());
            ps.setDouble(3, contract.getContractAmount());
            ps.setInt(4, contract.getCustomer().getId());
            ps.setInt(5, contract.getRealEstate().getId());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        }
        return false;
    }
}