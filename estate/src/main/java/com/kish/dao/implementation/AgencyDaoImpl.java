package com.kish.dao.implementation;
import com.kish.dao.AgencyDAO;
import com.kish.entity.Agency;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AgencyDaoImpl implements AgencyDAO {
    public static final String SELECT_QUERY = "SELECT id, name, address, phone_number FROM mydb.agency";
    public static final String CREATE = "INSERT INTO mydb.agency (name, address, phone_number) VALUES (?, ?, ?)";
    public static final String UPDATE_QUERY = "UPDATE mydb.agency SET phone_number= ?  WHERE ID= ?;";

    Connection connection;

    public AgencyDaoImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Agency> getAll() throws SQLException {
        List<Agency> listOfCAgencies = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_QUERY);
           // DBTablePrinter.printResultSet(rs);
            while (rs.next()) {
                Agency agency = extractAgencyFromResultSet(rs);
                listOfCAgencies.add(agency);
            }
        }
        return listOfCAgencies;
    }

    public Agency extractAgencyFromResultSet(ResultSet rs) throws SQLException {
        Agency agency = new Agency();
        agency.setId(rs.getInt("id"));
        agency.setName("name");
        agency.setAddress(rs.getString("address"));
        agency.setPhoneNumber(rs.getString("phone_number"));
        return agency;
    }

    @Override
    public boolean create(Agency agency) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setString(1, agency.getName());
            ps.setString(2, agency.getAddress());
            ps.setString(3, agency.getPhoneNumber());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean update(Agency agency) throws SQLException {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
                ps.setString(1, agency.getPhoneNumber());
                ps.setInt(2, agency.getId());
                int i = ps.executeUpdate();
                if (i == 1) {
                    return true;
                }
            }
            return false;
        }
    }

