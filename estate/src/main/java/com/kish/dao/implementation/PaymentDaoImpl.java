package com.kish.dao.implementation;

import com.kish.dao.PaymentDAO;
import com.kish.entity.Contract;
import com.kish.entity.Customer;
import com.kish.entity.Payment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PaymentDaoImpl implements PaymentDAO {
    Connection connection;

    public PaymentDaoImpl(Connection connection) {
        this.connection = connection;
    }

    public static final String SELECT_QUERY = "SELECT id, payment_amount, customer_id, contract_id from mydb.payment";
    public static final String SELECT_BY_CUSTOMER_ID = SELECT_QUERY + " WHERE customer_id= ?";
    public static final String SELECT_BY_CONTRACT_ID = SELECT_QUERY + " WHERE contract_id= ?";
    public static final String JOIN_ON_PAYMENT = "SELECT surname, payment_amount from mydb.customer inner\n" +
            " join mydb.payment on customer.id = payment.customer_id where customer.id is not null";


    public List<Payment> getPaymentsOnJoin() throws SQLException {
        List<Payment> payments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(JOIN_ON_PAYMENT)) {
            ResultSet rs = statement.executeQuery();
              while (rs.next()) {
                Payment payment = new Payment();
                payment.setCustomer(loadCustomer(rs));
                payment.setPaymentAmount(rs.getInt("payment_amount"));
                payments.add(payment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return payments;
    }

    public Customer loadCustomer(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setSurname(rs.getString("surname"));
        return customer;
    }

    @Override
    public List<Payment> getListOfPaymentsByContractID(Integer ID) throws SQLException {

        return getPayments(ID, SELECT_BY_CONTRACT_ID);
    }

    @Override
    public List<Payment> getListOfPaymentsByCustomerID(Integer ID) throws SQLException {
        return getPayments(ID, SELECT_BY_CUSTOMER_ID);
    }

    public List<Payment> getPayments(Integer ID, String SQLQuery) throws SQLException {
        List<Payment> listOfPayments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQLQuery)) {
            statement.setInt(1, ID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Payment payment = extractPaymentFromResultSet(rs);
                listOfPayments.add(payment);
            }
        }
        return listOfPayments;
    }


    @Override
    public List<Payment> getAll() throws SQLException {
        List<Payment> listOfCustomers = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_QUERY);
            while (rs.next()) {
                Payment payment = extractPaymentFromResultSet(rs);
                listOfCustomers.add(payment);
            }
        }
        return listOfCustomers;
    }

    public Payment extractPaymentFromResultSet(ResultSet rs) throws SQLException {

        Payment payment = new Payment();
        payment.setId(rs.getInt("id"));
        payment.setPaymentAmount(rs.getDouble("payment_amount"));
        Customer customer = new Customer();
        customer.setId(rs.getInt("customer_id"));
        payment.setCustomer(customer);
        Contract contract = new Contract();
        contract.setId(rs.getInt("contract_id"));
        payment.setContract(contract);
        return payment;
    }
}



