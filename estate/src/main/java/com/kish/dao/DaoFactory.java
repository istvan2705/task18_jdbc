package com.kish.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface DaoFactory {
    Connection getConnection() throws SQLException;

    interface DaoCreator {
        GeneralDao create(Connection connection);
    }
}
