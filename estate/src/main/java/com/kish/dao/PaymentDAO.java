package com.kish.dao;
import com.kish.entity.Payment;

import java.sql.SQLException;
import java.util.List;

public interface PaymentDAO extends GeneralDao<Payment> {
    List<Payment> getListOfPaymentsByCustomerID(Integer ID) throws SQLException;
    List<Payment> getListOfPaymentsByContractID(Integer ID) throws SQLException;
     List<Payment> getPaymentsOnJoin() throws SQLException;
}
