package com.kish.model;

import com.kish.DAO.Identified;

import java.util.Date;

public class Work implements Identified<Integer> {

    private String job;
    private Date startDate;
    private Date deadline;
    private Integer employeeID;
    private Integer projectID;

    public Integer getEmployeeID() {
        return employeeID;
    }

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    @Override
    public String toString() {
        return "Work{" +
                "job='" + job + '\'' +
                ", startDate=" + startDate +
                ", deadline=" + deadline +
                ", employeeID=" + employeeID +
                ", projectID=" + projectID +
                '}';
    }

    @Override
    public Integer getId() {
        return null;
    }
}

