package com.kish.DAO.implementations;

import com.kish.DAO.AbstractJDBCDao;
import com.kish.DAO.DBException;
import com.kish.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ProjectDAOImpl extends AbstractJDBCDao<Project, Integer> {

    public static final String SELECT_QUERY = "SELECT ID, project_name, budget, employee_ID  FROM mydb.project";
    public static final String CREATE_QUERY = "INSERT INTO mydb.project (project_name, budget, employee_ID) \n" +
            "VALUES (?, ?, ?);";
    public static final String UPDATE_QUERY = "UPDATE mydb.project SET project_name=?, budget= ?, employee_ID= ? \n"+
            "WHERE ID= ?;";
    public static final String DELETE_QUERY = "DELETE FROM mydb.project WHERE ID= ?;";

    public ProjectDAOImpl(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY ;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected List<Project> parseResultSet(ResultSet rs) throws DBException {
        List<Project> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Project project = new Project();
                project.setID(rs.getInt("ID"));
                project.setProjectName(rs.getString("project_name"));
                project.setBudget(rs.getInt("budget"));
                project.setEmployeeID(rs.getInt("employee_ID"));
                result.add(project);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Project object) throws DBException {
        try {
            statement.setString(2, object.getProjectName());
            statement.setInt(3, object.getBudget());
            statement.setInt(4, object.getEmployeeID());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Project object) throws DBException {
        try {
            statement.setString(1, object.getProjectName());
            statement.setInt(2, object.getBudget());
            statement.setInt(3, object.getEmployeeID());
            statement.setInt(4, object.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
