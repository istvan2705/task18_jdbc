package com.kish.DAO.implementations;

import com.kish.DAO.AbstractJDBCDao;
import com.kish.DAO.DBException;
import com.kish.model.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class EmployeeDAOImpl extends AbstractJDBCDao<Employee, Integer> {

    public static final String SELECT_QUERY = "SELECT ID, last_name, name, department_ID FROM mydb.employee";
    public static final String CREATE_QUERY = "INSERT INTO mydb.employee (last_name, name, department_ID) \n"+
    "VALUES (?, ?, ?);";
    public static final String UPDATE_QUERY = "UPDATE mydb.employee SET last_name=?, name= ?, department_ID = ? \n"+
    "WHERE ID= ?;";
    public static final String DELETE_QUERY = "DELETE FROM mydb.employee WHERE ID= ?;";

    public EmployeeDAOImpl(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected List<Employee> parseResultSet(ResultSet rs) throws DBException {
        List<Employee> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Employee employee = new Employee();
                employee.setID(rs.getInt("ID"));
                employee.setName(rs.getString("last_name"));
                employee.setLastName(rs.getString("name"));
                employee.setDepartmentID(rs.getInt("department_ID"));
                result.add(employee);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Employee object) throws DBException {
        try {
            statement.setString(2, object.getLastName());
            statement.setString(3, object.getName());
            statement.setInt(4, object.getDepartmentID());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Employee object) throws DBException {
        try {
            statement.setString(1, object.getName());
            statement.setString(2, object.getLastName());
            statement.setInt(3, object.getDepartmentID());
            statement.setInt(4, object.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
