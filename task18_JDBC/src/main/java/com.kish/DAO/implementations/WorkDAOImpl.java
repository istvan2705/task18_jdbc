package com.kish.DAO.implementations;

import com.kish.DAO.AbstractJDBCDao;
import com.kish.DAO.DBException;
import com.kish.model.Work;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class WorkDAOImpl extends AbstractJDBCDao<Work, Integer> {

    public static final String SELECT_QUERY = "SELECT job, start_date, deadline, employee_ID, project_ID FROM mydb.work";
    public static final String CREATE_QUERY = "INSERT INTO mydb.work (job, start_date, deadline, employee_ID, project_ID) \n" +
            "VALUES (?, ?, ?, ?, ?);";
    public static final String UPDATE_QUERY = "UPDATE mydb.work SET job= ?, start_date= ?, \n" +
            " deadline= ?, employee_ID= ? WHERE project_ID= ?;";
    public static final String DELETE_QUERY = "DELETE FROM mydb.work WHERE ID= ?;";
    public WorkDAOImpl(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected List<Work> parseResultSet(ResultSet rs) throws DBException {
        List<Work> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Work work = new Work();
                work.setJob(rs.getString("job"));
                work.setStartDate(rs.getDate("start_date"));
                work.setDeadline(rs.getDate("deadline"));
                work.setEmployeeID(rs.getInt("employee_ID"));
                work.setProjectID(rs.getInt("project_ID"));
                result.add(work);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Work object) throws DBException {
        try {
            statement.setString(1, object.getJob());
            statement.setDate(2, (Date) object.getStartDate());
            statement.setDate(3, (Date) object.getDeadline());
            statement.setInt(4, object.getEmployeeID());
            statement.setInt(5, object.getProjectID());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Work object) throws DBException {
        try {
            statement.setString(1, object.getJob());
            statement.setDate(2, (Date) object.getStartDate());
            statement.setDate(3, (Date) object.getDeadline());
            statement.setInt(4, object.getEmployeeID());
            statement.setInt(5, object.getProjectID());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
