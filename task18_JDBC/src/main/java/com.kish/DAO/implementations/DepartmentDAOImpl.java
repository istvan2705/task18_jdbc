package com.kish.DAO.implementations;

import com.kish.DAO.AbstractJDBCDao;
import com.kish.DAO.DBException;
import com.kish.model.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class DepartmentDAOImpl extends AbstractJDBCDao<Department, Integer> {
    public static final String SELECT_QUERY = "SELECT ID, name, location FROM mydb.department";
    public static final String CREATE_QUERY = "INSERT INTO mydb.department (name, location) VALUES (?, ?);";
    public static final String UPDATE_QUERY = "UPDATE mydb.department SET name= ?, location = ? WHERE ID= ?;";
    public static final String DELETE_QUERY = "DELETE FROM mydb.department WHERE ID= ?;";

    public DepartmentDAOImpl(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT_QUERY;
    }

    @Override
    public String getCreateQuery() {
        return CREATE_QUERY;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE_QUERY;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE_QUERY;
    }

    @Override
    protected List<Department> parseResultSet(ResultSet rs) throws DBException {
        List<Department> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Department department = new Department();
                department.setID(rs.getInt("ID"));
                department.setName(rs.getString("name"));
                department.setLocation(rs.getString("location"));
                result.add(department);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Department object) throws DBException {
        try {
            statement.setString(1, object.getName());
            statement.setString(2, object.getLocation());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Department object) throws DBException {
        try {
            statement.setString(1, object.getName());
            statement.setString(2, object.getLocation());
            statement.setInt(3, object.getId());
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
